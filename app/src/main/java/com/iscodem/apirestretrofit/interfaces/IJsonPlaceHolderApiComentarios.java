package com.iscodem.apirestretrofit.interfaces;

import com.iscodem.apirestretrofit.model.Comentarios;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface IJsonPlaceHolderApiComentarios {

    //Metodo encargado de obtener la informacion del API REST
    @GET("comentarios")
    Call<List<Comentarios>> getComentarios();
}
