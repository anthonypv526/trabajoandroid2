package com.iscodem.apirestretrofit;

import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.iscodem.apirestretrofit.interfaces.IJsonPlaceHolderApiComentarios;
import com.iscodem.apirestretrofit.model.Comentarios;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class SecondActivity extends AppCompatActivity {
    public static final String TAG = SecondActivity.class.getSimpleName();

    TextView txtComentariosJson;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.second_activity);
        Log.d(TAG,">>>Metodo onCreate()");

        txtComentariosJson = findViewById(R.id.txtComentariosJson);

        this.getPost(); //LLamada al retrofit
    }


    private void getPost(){
        Log.d(TAG,">>>Metodo SecondActivity.getPost()");
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://jsonplaceholder.typicode.com/comments")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        IJsonPlaceHolderApiComentarios iJsonPlaceHolderApiComentarios = retrofit.create(IJsonPlaceHolderApiComentarios.class);

        Call<List<Comentarios>> call = iJsonPlaceHolderApiComentarios.getComentarios();

        call.enqueue(new Callback<List<Comentarios>>() {
            @Override
            public void onResponse(Call<List<Comentarios>> call, Response<List<Comentarios>> response) {
                Log.d(TAG,">>>Metodo call.enqueue.onResponse()");
                if(!response.isSuccessful()){
                    txtComentariosJson.setText("Codigo ok: "+response.code());
                    return;
                }

                List<Comentarios> lsPost = response.body();

                String contenido = "";

                for(Comentarios post : lsPost){
                    contenido+="postId"+post.getPostId()+"\n";
                    contenido+="id"+post.getId()+"\n";
                    contenido+="name"+post.getName()+"\n";
                    contenido+="email"+post.getEmail()+"\n";
                    contenido+="body"+post.getBody()+"\n";

                    txtComentariosJson.append(contenido);
                }
            }

            @Override
            public void onFailure(Call<List<Comentarios>> call, Throwable t) {
                Log.d(TAG,">>>Metodo call.enqueue.onFailure()");
                txtComentariosJson.setText("ERROR ok: "+t.getMessage());
            }
        });
    }
}
